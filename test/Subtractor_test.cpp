#include "../src/Subtractor.cpp"
#include <iostream>
using namespace std;

int main(int argc, char **argv)
{
    Subtractor second_static(20,3);
    Subtractor third_static(20,1);
    Subtractor fourth_static(20,5);
 
    Subtractor temp;
    temp = third_static - second_static;

    Subtractor first_adder;
    first_adder.set_operand_1(10);
    first_adder.set_operand_2(5);
    first_adder.print();

    cout << first_adder - 100 << endl;
    cout  << "Minus operator test" << endl;
    temp.print();
    second_static.print();
    third_static.print();
    cout << "Testing operator() for fourth static" << endl;
    cout << fourth_static() << endl;
    cout << fourth_static << endl;

    cout << "Testing copy constructor\n";
    Subtractor cp_constructor(fourth_static);
    if(cp_constructor.sub() == 15)
    {
        cout << "Copy constructor passed\n";
    }
    else
    {
        cout << "Failure in copy constructor\n";
    }
    cout << endl << "End of test." << endl;
    cout << "Testing of assignment operator\n";
    Subtractor tm(2,2);
    tm = cp_constructor;
    if(tm.sub() != 0)
    {
        cout << "Assignment operator operation passed!\n";
    }
    else
    {
        cout << "Failure in assignment operator test" << endl;
    }
    return 0;
}