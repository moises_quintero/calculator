#include "../src/Calculator.cpp"
#include <iostream>
using namespace std;

int main(int argc, char **argv)
{
    Calculator user_if;
    user_if.set_operation_option(5);
    cout << "NONE: -1 Should be displayed on the following line." << endl;
    cout << user_if.result() << endl << endl;

    Calculator user_if_two(3,2);
    user_if_two.set_operation_option(1);
    cout << "Addition operation of 3 and 2 = "<< user_if_two.result();
    cout << endl;

    Calculator user_if_subber(25,6);
    user_if_subber.set_operation_option(2);
    cout << "Difference of items:" << user_if_subber.result() << endl;

    cout << "\nTesting copy constructor\n";
    Calculator cp_data(user_if_subber);
    if(cp_data.result() == 19)
        cout << "Copy Constructor Passed!\n";
    else
        cout << "Failure in Copy constructor\n";

    cout << endl << "Testing assignment operator.\n";
    Calculator acp_object;
    acp_object = cp_data;
    if(acp_object.result() == 19)
        cout << "Copy Constructor Passed!\n";
    else
        cout << "Failure in Copy constructor value of acp_object.result() " << acp_object.result() << "\n";

    return 0;
}