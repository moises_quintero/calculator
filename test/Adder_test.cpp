#include "../src/Adder.cpp"
#include <iostream>
using namespace std;

int main(int argc, char **argv)
{
    Adder first_adder, normal_adder(4,4);
    cout << "Testing sum of default constructor is 0\n";
    if(first_adder.sum() == 0)
        cout <<"Default test pass\n";
    else
        cout << "Failure in default sum operands of default constructor\n";
    cout << endl;
    cout << "Testing sum of constructor with two parameters is 8\n";
    if(normal_adder.sum() == 8)
        cout << "Two parameter constructor pass" << endl;
    else
        cout << "Failure in two parameter constructor" <<endl;

    cout << "Testing copy constructor\n";
    Adder cp_constructor(normal_adder);
    if(cp_constructor.sum() == 8)
        cout << "Copy constructor pass" << endl;
    else
        cout << "Failure in copy constructor" <<endl;
 
    Adder copy_by_assignment;
    copy_by_assignment = cp_constructor;
    if(copy_by_assignment.sum() == 8)
        cout << "Assignment constructor pass" << endl;
    else
        cout << "Failure in assignment constructor" <<endl;
    cout << "End of test" << endl;
    
    return 0;
}