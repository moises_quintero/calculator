#include "../h/Adder.h"
#include <iostream>

const Adder& Adder::operator=(const Adder& copy_object)
{
    if(this != &copy_object)
    {
        set_operand_1(copy_object.get_operand_1());
        set_operand_2(copy_object.get_operand_2());
    }

    return *this;
}

Adder::Adder(const Adder& copy_object)
{
    this->a = copy_object.a;
    this->b = copy_object.a;
}

Adder::Adder()
{
    a=0;
    b=0;
}

Adder::~Adder()
{
    a=0;
    b=0;
}

Adder::Adder(int oa, int ob)
{
    a = oa;
    b = ob;
}

void Adder::print() const
{
    std::cout << "Sum of a+b=" << sum() << std::endl;
}

int Adder::sum() const
{
    return (a+b);
}

void Adder::set_operand_1(const int& va)
{
    a = va;
}

void Adder::set_operand_2(const int& va)
{
    b = va;
}

int Adder::get_operand_1() const
{
    return a;
}

int Adder::get_operand_2() const
{
    return b;
}