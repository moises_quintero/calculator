#include "../h/Calculator.h"

Calculator::Calculator(const Calculator& cp_object)
{
    this->operation_option = cp_object.operation_option;
    this->add_option = cp_object.add_option;
    this->sub_option = cp_object.sub_option;
}

const Calculator& Calculator::operator=(const Calculator& cp_object)
{
    if(this != &cp_object)
    {
        this->operation_option = cp_object.operation_option;
        this->add_option = cp_object.add_option;
        this->sub_option = cp_object.sub_option;
    }
    return *this;
}

Calculator::Calculator(int a, int b): add_option(a,b), sub_option(a,b)
{
    operation_option = 0;    
}

Calculator::Calculator()
{
    add_option.set_operand_1(0);
    add_option.set_operand_2(0);
    
    sub_option.set_operand_1(0);
    sub_option.set_operand_2(0);
}

Calculator::~Calculator()
{

}

void Calculator::set_operation_option(int v)
{
    operation_option = v;
}

int Calculator::result() const 
{
    switch(operation_option)
    {
        case 1:
            return add_option.sum();
        case 2:
            return sub_option.sub();
        default:
            return -1;
            break;
    }

    return 0;
}