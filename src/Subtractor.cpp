#include "../h/Subtractor.h"
#include <iostream>
using namespace std;

const Subtractor& Subtractor::operator=(const Subtractor& copy_object)
{
    if(this != &copy_object)
    {
        this->a = copy_object.get_operand_1();
        this->b = copy_object.get_operand_2();
    }
    return *this;
}

Subtractor::Subtractor(const Subtractor& copy_object)
{
    this->a = copy_object.get_operand_1();
    this->b = copy_object.get_operand_2();
}

Subtractor::Subtractor()
{
    this->a = 0;
    this->b = 0;
}

Subtractor::Subtractor(int oa, int ob)
{
    this->a = oa;
    this->b = ob;
}

Subtractor::~Subtractor()
{
    this->a = 0;
    this->b = 0;
}

int Subtractor::sub() const
{
    return this->a - this->b;
}

void Subtractor::print() const
{
    cout << "Difference value: " << sub() << endl;
}

void Subtractor::set_operand_1(int oa)
{
    this->a = oa;
}

void Subtractor::set_operand_2(int ob)
{
    this->b = ob;
}

int operator-(const Subtractor& ref, int oa)
{
    return ((ref.a *100) - ref.sub() - oa);
}

Subtractor operator-(const Subtractor& lref, const Subtractor& rref)
{
    Subtractor temp;
    temp.a = lref.a - rref.a;
    temp.b = lref.b - rref.b;

    return temp;
}

int Subtractor::get_operand_1() const
{
    return this->a;
}

int Subtractor::get_operand_2() const
{
    return this->b;
}

ostream& operator<<(ostream& os, const Subtractor& ref)
{
    os << "A: " << ref.get_operand_1() << " B: " << ref.get_operand_2() << endl;
    return os;
}

int Subtractor::operator()(void)
{
    return this->a - this->b;
}