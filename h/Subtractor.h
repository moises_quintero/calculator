#ifndef SUBTRACTOR_H
#define SUBTRACTOR_H
#include <iostream>

class Subtractor
{
    public:
        const Subtractor& operator=(const Subtractor&);
        Subtractor(const Subtractor&);
        Subtractor();
        Subtractor(int,int);
        ~Subtractor();

        int sub() const;
        void print() const;
        void set_operand_1(int);
        void set_operand_2(int);
        int operator()(void);
        int get_operand_1() const;
        int get_operand_2() const;

        friend Subtractor operator-(const Subtractor&, const Subtractor&);  
        friend int operator-(const Subtractor& ref, int);
        friend std::ostream& operator<<(std::ostream&, const Subtractor&);
    private:
        int a;
        int b;
};

#endif