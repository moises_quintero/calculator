#ifndef ADDER_H
#define ADDER_H

class Adder
{
    public:
       
        Adder();
        Adder(int, int);
        Adder(const Adder&);

        const Adder& operator=(const Adder&);
        
        void print() const;
        int sum() const;
        void set_operand_1(const int&);
        void set_operand_2(const int&);
        int get_operand_1() const;
        int get_operand_2() const;
        
        ~Adder();
    private:
        int a;
        int b;
};

#endif