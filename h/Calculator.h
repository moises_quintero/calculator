#ifndef CALCULATOR_H
#define CALCULATOR_H
#include "../src/Adder.cpp"
#include "../src/Subtractor.cpp"

class Calculator
{
    public:
        ~Calculator();
        Calculator();
        Calculator(int, int );
        Calculator(const Calculator&);
        const Calculator& operator=(const Calculator& );
        void set_operation_option(int v);
        int result() const;
    private:
        int operation_option;
        Adder add_option;
        Subtractor sub_option;
};

#endif